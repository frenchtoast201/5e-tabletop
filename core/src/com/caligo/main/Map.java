package com.caligo.main;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Path;

public class Map {

    String mapName;
    File mapFile;

    public void createNewFile(String title, int number){
        mapName =  "maps\\map"+number+".txt";
        mapFile = new File(mapName);

        try{
            if(mapFile.createNewFile()) {
                FileWriter fileWriter = new FileWriter(mapName);
                PrintWriter printWriter = new PrintWriter(fileWriter);

                //add title and empty values for map data
                printWriter.println(title);
                printWriter.println("0");
                printWriter.println("0");
                printWriter.close();
            }
        }catch(Exception Exception){
            System.out.println("File Creation Failed");
        }
    }
}

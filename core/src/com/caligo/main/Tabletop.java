package com.caligo.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Tabletop implements Screen, InputProcessor {

    final Main game;                                          
    private OrthographicCamera camera;
    private Viewport viewport;
    private List<String> mapData;
    public List<String> tokenData;
    private String title;
    private int mapWidth;
    private int mapHeight;
    private boolean tokenMenu;

    private SpriteBatch menuBatch;
    private Camera menuCam;
    private Viewport menuView;

    private Texture mapImage;
    private Texture backButton;
    private Texture black;
    private Texture addButton;
    private Texture menuBar;

    private Sound click;

    public Tabletop (final Main gam, int mapNo){
        game = gam;
        camera = new OrthographicCamera();
        menuCam = new OrthographicCamera();
        viewport = new ScreenViewport(camera);
        menuView = new ScreenViewport(menuCam);

        tokenMenu = false;

        menuBatch = new SpriteBatch();
        black = new Texture("Black.png");
        backButton = new Texture("Back.PNG");
        addButton = new Texture("Add.png");
        menuBar = new Texture("Menu Bar.png");

        click = Gdx.audio.newSound(Gdx.files.internal("click.wav"));

        if(Files.exists(Paths.get("map" + mapNo+ ".png"))){
            mapImage = new Texture("map" + mapNo+ ".png");
        }else if(Files.exists(Paths.get("map" + mapNo+ ".jpg"))){
            mapImage = new Texture("map" + mapNo+ ".jpg");
        }else{
            mapImage = new Texture("Blank.png");
        }

        try {
            mapData = Files.readAllLines(Paths.get("maps\\map" + mapNo + ".txt"));
        }catch (Exception Exception) {
            System.out.println("Attempted to read nonexistent data");
        }

        title = mapData.get(0);
        mapWidth = Integer.parseInt(mapData.get(1));
        mapHeight = Integer.parseInt(mapData.get(2));

        try {
            tokenData = Files.readAllLines(Paths.get("tokens\\Token Data.txt"));
        }catch (Exception Exception) {
            System.out.println("Attempted to read nonexistent data");
        }

        Gdx.input.setInputProcessor(this);

        camera.zoom = 3;
        camera.position.add(mapWidth*50, mapHeight*50, 0);

    }

    public void render(float delta){
        Gdx.gl.glClearColor(128/255f, 128/255f, 128/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        menuCam.update();

        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(mapImage, 0, 0, mapWidth*100, mapHeight*100);
        if(camera.zoom<=5) {
            for (int i = 0; i < mapWidth; i++) {
                game.batch.draw(black, i * 100, 0, 3, mapHeight * 100);
            }
            for (int i = 0; i < mapHeight; i++) {
                game.batch.draw(black, 0, i * 100, mapWidth * 100, 3);
            }
        }
        game.batch.end();

        menuBatch.setProjectionMatrix(menuCam.combined);
        menuBatch.begin();
        menuBatch.draw(backButton, -Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/2f-30, 100, 30);
        if(!tokenMenu){
            menuBatch.draw(addButton, Gdx.graphics.getWidth()/2f-75, Gdx.graphics.getHeight()/2f-45, 75, 45);
        }
        else{//token menu is open
            loadTokenMenu(menuBatch, tokenData);
        }
        menuBatch.end();


    }

    public void loadTokenMenu(SpriteBatch batch, List<String> tokenData){
        batch.draw(menuBar, Gdx.graphics.getWidth()/2f-150, -Gdx.graphics.getHeight()/2f, 150, Gdx.graphics.getHeight());
        for(int i = 0; i< Integer.parseInt(tokenData.get(1)) ; i++){
       //     batch.draw()
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        menuView.update(width, height);
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        mapImage.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(screenX<100&&screenY<30){
            click.play();
            game.setScreen(new LoadMapMenu(game));
            dispose();
        }
        else if(screenX>Gdx.graphics.getWidth()-75&&screenY<45){
            click.play();
            tokenMenu = !tokenMenu;
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        float x = Gdx.input.getDeltaX();
        float y = Gdx.input.getDeltaY();

        camera.translate(-x*camera.zoom,y*camera.zoom);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if(camera.zoom+amount>0) {
            camera.zoom += amount;
        }
        return true;
    }
}

package com.caligo.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class LoadMapMenu implements Screen {
    final Main game;
    OrthographicCamera camera;
    private Texture backArrow;
    private Rectangle backBox;

    private Texture map1;
    private Texture map2;
    private Texture map3;
    private Texture map4;
    private Texture map5;
    private Texture map6;
    private Texture map7;
    private Texture map8;
    private Texture map9;
    private Texture map10;
    private Texture map11;
    private Texture map12;
    private Texture map13;
    private Texture map14;
    private Texture map15;
    private ArrayList<Texture> mapThumbs;

    private Rectangle mapBox1;
    private Rectangle mapBox2;
    private Rectangle mapBox3;
    private Rectangle mapBox4;
    private Rectangle mapBox5;
    private Rectangle mapBox6;
    private Rectangle mapBox7;
    private Rectangle mapBox8;
    private Rectangle mapBox9;
    private Rectangle mapBox10;
    private Rectangle mapBox11;
    private Rectangle mapBox12;
    private Rectangle mapBox13;
    private Rectangle mapBox14;
    private Rectangle mapBox15;
    private ArrayList<Rectangle> mapBoxes;

    int mapRecentClicked;

    Sound click;

    public LoadMapMenu(final Main gam) {
        game = gam;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        backArrow = new Texture("Back.png");

        backBox = new Rectangle();
        backBox.x = 0;
        backBox.y = 0;
        backBox.width = 100;
        backBox.height = 30;

        //<editor-fold desc="Load map thumbs and hitboxes">
        mapThumbs = new ArrayList<Texture>();
        mapThumbs.add(map1);
        mapThumbs.add(map2);
        mapThumbs.add(map3);
        mapThumbs.add(map4);
        mapThumbs.add(map5);
        mapThumbs.add(map6);
        mapThumbs.add(map7);
        mapThumbs.add(map8);
        mapThumbs.add(map9);
        mapThumbs.add(map10);
        mapThumbs.add(map11);
        mapThumbs.add(map12);
        mapThumbs.add(map13);
        mapThumbs.add(map14);
        mapThumbs.add(map15);
        loadMapThumbs(1, mapThumbs);

        mapBoxes = new ArrayList<Rectangle>();
        mapBoxes.add(mapBox1);
        mapBoxes.add(mapBox2);
        mapBoxes.add(mapBox3);
        mapBoxes.add(mapBox4);
        mapBoxes.add(mapBox5);
        mapBoxes.add(mapBox6);
        mapBoxes.add(mapBox7);
        mapBoxes.add(mapBox8);
        mapBoxes.add(mapBox9);
        mapBoxes.add(mapBox10);
        mapBoxes.add(mapBox11);
        mapBoxes.add(mapBox12);
        mapBoxes.add(mapBox13);
        mapBoxes.add(mapBox14);
        mapBoxes.add(mapBox15);
        loadMapBoxes(1, mapBoxes);
        //</editor-fold>

        click = Gdx.audio.newSound(Gdx.files.internal("click.wav"));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(218/255f, 165/255f, 32/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Gdx.graphics.setWindowedMode(800, 480);
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.font.setColor(Color.BLACK);
        game.font.draw(game.batch, "Load Map", 300, 430);
        game.batch.draw(backArrow, 0, 450, 100, 30);

        for(int i=0; i<mapThumbs.size(); i++){
            if(i<5){
                game.batch.draw(mapThumbs.get(i), (150*i)+40, 225, 125, 75);
            }
            else if(i<10){
                game.batch.draw(mapThumbs.get(i), (150*(i-5))+40, 125, 125, 75);
            }
            else{
                game.batch.draw(mapThumbs.get(i), (150*(i-10))+40, 25, 125, 75);
            }
        }

        game.batch.end();

        if (Gdx.input.isTouched()){
            Vector2 touchPos = new Vector2();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY());

            if(backBox.contains(touchPos)){
                click.play();
                game.setScreen(new Menu(game));
                dispose();
            }

            for(int i = 0; i<mapBoxes.size(); i++){
                if(mapBoxes.get(i).contains(touchPos)){
                    if(Files.exists(Paths.get("maps/map"+(i+1)+".txt"))) {
                        click.play();
                        game.setScreen(new Tabletop(game, (i + 1)));
                        dispose();
                    }
                }
            }
        }
    }

    private void loadMapThumbs(int page, ArrayList mapThumbs){
        for (int i = 0; i<mapThumbs.size(); i++){
            if(Files.exists(Paths.get("map"+(i+1)+".png"))){
                mapThumbs.set(i, new Texture("map"+(i+1)+".png"));
            }
            else if(Files.exists(Paths.get("map"+(i+1)+".jpg"))){
                mapThumbs.set(i, new Texture("map"+(i+1)+".jpg"));
            }
            else{
                mapThumbs.set(i, new Texture("Blank.png"));
            }
        }

    }

    private void loadMapBoxes(int page, ArrayList<Rectangle> mapBoxes){
        for (int i = 0; i<mapBoxes.size(); i++){
            mapBoxes.set(i, new Rectangle());
            mapBoxes.get(i).height = 75;
            mapBoxes.get(i).width = 125;
            if(i<5){
                mapBoxes.get(i).x = (150*i)+40;
                mapBoxes.get(i).y = 480-225-75;
            }
            else if(i<10){
                mapBoxes.get(i).x =(150*(i-5))+40;
                mapBoxes.get(i).y = 480-125-75;
            }
            else{
                mapBoxes.get(i).x =(150*(i-10))+40;
                mapBoxes.get(i).y = 480-25-75;
            }
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        backArrow.dispose();
        for (Texture tex : mapThumbs){
            tex.dispose();
        }
    }
}

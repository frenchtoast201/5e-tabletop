package com.caligo.main;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class ScreenTemplate implements Screen {

    final Main game;
    private OrthographicCamera camera;

    public ScreenTemplate(final Main gam){
        game = gam;
        camera = new OrthographicCamera();

    }

    public void render(float delta){

    }

    @Override
    public void resize(int width, int height) {
        camera.viewportHeight = height;
        camera.viewportWidth = width;
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
}

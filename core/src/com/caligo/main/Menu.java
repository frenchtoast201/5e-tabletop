package com.caligo.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Menu implements Screen {
    final Main game;
    private OrthographicCamera camera;
    private Rectangle editMapButton;
    private Rectangle loadMapButton;
    private Rectangle editTokenButton;
    private Texture editMapSprite;
    private Texture loadMapSprite;
    private Texture editTokenSprite;
    private Sound click;

    public Menu(final Main gam){
        game = gam;



        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        editMapButton = new Rectangle();
        editMapButton.x = 50;
        editMapButton.y = 480-150-64;
        editMapButton.width = 128;
        editMapButton.height = 64;

        loadMapButton = new Rectangle();
        loadMapButton.x = 400-128;
        loadMapButton.y = 480-(150-32)-64*2;
        loadMapButton.width = 128*2;
        loadMapButton.height = 128;

        editTokenButton = new Rectangle();
        editTokenButton.x = 800-50-128;
        editTokenButton.y = 480-150-64;
        editTokenButton.width = 128;
        editTokenButton.height = 64;

        editMapSprite = new Texture("Edit Maps Button.png");
        loadMapSprite = new Texture("Load Maps.png");
        editTokenSprite = new Texture("Edit Tokens.png");

        click = Gdx.audio.newSound(Gdx.files.internal("click.wav"));

        game.font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }


    public void render(float delta){
        Gdx.gl.glClearColor(.8f, .7f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Gdx.graphics.setWindowedMode(800, 480);
        camera.update();

        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.font.setColor(Color.BLACK);
        game.font.getData().setScale(3);
        game.font.draw(game.batch, "Welcome to 5e Map! ", 200, 400);
        game.font.draw(game.batch, "Tap a button to start!", 200, 350);
        game.batch.draw(editMapSprite, 50, 150, 128, 64);
        game.batch.draw(loadMapSprite, 400-128, 150-32, 128*2, 64*2);
        game.batch.draw(editTokenSprite, 800-128-50, 150, 128, 64);

        game.batch.end();

        if (Gdx.input.isTouched()){
            Vector2 touchPos = new Vector2();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY());

            if(editMapButton.contains(touchPos)){
                click.play();
                game.setScreen(new EditMapsMenu(game));
                dispose();
            }
            else if(loadMapButton.contains(touchPos)){
                click.play();
                game.setScreen(new LoadMapMenu(game));
                dispose();
            }

            else if(editTokenButton.contains(touchPos)){
                click.play();
                game.setScreen(new EditTokensMenu(game));
                dispose();
            }
        }

    }

    @Override
    public void resize(int width, int height) {
        camera.viewportHeight = height;
        camera.viewportWidth = width;
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        loadMapSprite.dispose();
        editMapSprite.dispose();
        editTokenSprite.dispose();
    }
}

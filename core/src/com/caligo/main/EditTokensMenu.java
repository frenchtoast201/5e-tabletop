package com.caligo.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class EditTokensMenu implements Screen {
    final Main game;
    OrthographicCamera camera;

    private Texture backArrow;
    private Rectangle backBox;

    Sound click;

    public EditTokensMenu(final Main gam) {
        game = gam;

        backArrow = new Texture("Back.png");

        backBox = new Rectangle();
        backBox.x = 0;
        backBox.y = 0;
        backBox.width = 100;
        backBox.height = 30;

        click = Gdx.audio.newSound(Gdx.files.internal("click.wav"));

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(173/255f, 200/255f, 47/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Gdx.graphics.setWindowedMode(800, 480);
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.font.setColor(Color.BLACK);
        game.font.draw(game.batch, "Edit Tokens", 300, 430);
        game.batch.draw(backArrow, 0, 450, 100, 30);
        game.batch.end();

        if (Gdx.input.isTouched()) {
            Vector2 touchPos = new Vector2();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY());

            if (backBox.contains(touchPos)) {
                click.play();
                game.setScreen(new Menu(game));
                dispose();
            }
        }

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        click.dispose();
        backArrow.dispose();
    }
}

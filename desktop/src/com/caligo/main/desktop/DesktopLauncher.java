package com.caligo.main.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.caligo.main.Main;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "D&D 5e Tabletop Map";
		config.height = 480;
		config.width = 800;
		new LwjglApplication(new Main(), config);
	}
}
